var searchData=
[
  ['getbufferframeid',['getBufferFrameID',['../classljp_1_1_lib_ljp.html#ae353f1d1d9691c7e022f5da31b07bdc3',1,'ljp::LibLjp']]],
  ['getbufferheight',['getBufferHeight',['../classljp_1_1_lib_ljp.html#ae0e70a8843c3f169fae30caaf6cf013e',1,'ljp::LibLjp']]],
  ['getbufferwidth',['getBufferWidth',['../classljp_1_1_lib_ljp.html#a50d2a976a1bf93d2197817e5c8de6a2e',1,'ljp::LibLjp']]],
  ['getcolorat',['getColorAt',['../classljp_1_1_lib_ljp.html#a59b3e3bbf6beaf71a07c0ce94efce184',1,'ljp::LibLjp']]],
  ['getdepthbuffer',['getDepthBuffer',['../classljp_1_1_ljp_device.html#a401b1ea82e014ab9cd48114813641aea',1,'ljp::LjpDevice']]],
  ['getdrawingmodule',['getDrawingModule',['../classljp_1_1_lib_ljp.html#af19f2263c22691ea889f07de020b56fc',1,'ljp::LibLjp']]],
  ['getkinectbuffer',['getKinectBuffer',['../classljp_1_1_lib_ljp.html#adde2435965e5b829256baf9970be2991',1,'ljp::LibLjp']]],
  ['getrgbabuffer',['getRGBABuffer',['../classljp_1_1_drawing.html#a84b0d961e15edc305b11a0df56d12d6d',1,'ljp::Drawing']]],
  ['getrunning',['getRunning',['../classljp_1_1_drawing.html#a9bc5aae44667f693b59da162d94812f9',1,'ljp::Drawing']]],
  ['getstoprunningthelibwhendrawingmodulerunningandwindowclosed',['getStopRunningTheLibWhenDrawingModuleRunningAndWindowClosed',['../classljp_1_1_drawing.html#aa34cc55fac100b47980409cf7985c4e2',1,'ljp::Drawing']]],
  ['getvideobuffer',['getVideoBuffer',['../classljp_1_1_ljp_device.html#aa86723caf147cd693598a32d941574b5',1,'ljp::LjpDevice']]],
  ['getwindow',['getWindow',['../classljp_1_1_drawing.html#a93e612c39fafd36ec91590c6ab2ae2df',1,'ljp::Drawing']]]
];
