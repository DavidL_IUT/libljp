package libljp;

import java.nio.ByteBuffer;

import org.openkinect.freenect.Context;
import org.openkinect.freenect.DepthHandler;
import org.openkinect.freenect.Device;
import org.openkinect.freenect.FrameMode;
import org.openkinect.freenect.Freenect;
import org.openkinect.freenect.VideoHandler;
/**
 * <b> La classe LibLjp est la classe principale du projet version Java.</b>
 * 
 * Elle crée le context (fournie avec la lib) et détecte la Kinect.
 * @author Groupe IHM à l'aide d'un Kinect.
 * @version 1.0
 */
public class LibLjp{
	
	/**
	 * Le context crée par la lib.
	 * @see org.openkinect.freenect.Context
	 */
	private Context ctx;
	
	/**
	 * Le périphérique Kinect, qui est ici représenté par l'objet Device.
	 * @see org.openkinect.freenect.Device
	 */
	private Device device;
	
	/**
	 * La largeur du buffer.
	 */
	private int bufferWidth;
	
	/**
	 * La hauteur du buffer.
	 */
	private int bufferHeight;
	
	/**
	 * Tableau de byte pour la vidéo.
	 */
	private ByteBuffer kinectVideoBuffer;
	
	/**
	 * Tableau de byte pour la profondeur.
	 */
	private ByteBuffer kinectDepthBuffer;
	
	/**
	 * L'ID de la frame vidéo.
	 */
	private int videoBufferFrameID;
	
	/**
	 * L'ID de la frame de profondeur.
	 */
	private int depthBufferFrameID;
	
	/**
	 * Booléen qui précise si l'application tourne ou non.
	 */
	private boolean running;
	
	/**
	 * Constructeur LibLjp.
	 * 
	 * <p>A la construction d'un objet LibLjp, on déclare la taille du buffer (hauteur et largeur), on met le booléen running à true et on defini les ID à 0.
	 * De plus on crée le context et on détecte si la Kinect est connectée.
	 * </p>
	 */
	LibLjp(){
		bufferWidth = 640;
		bufferHeight = 480;
		videoBufferFrameID = 0;
		depthBufferFrameID = 0;
		running = true;
	    
	    // Create the context object
		ctx = Freenect.createContext();
        if(ctx.numDevices() > 0){
        	// Create the device
            device = ctx.openDevice(0);
        }
        else{
            System.err.println("WARNING: No kinects detected, hardware tests will be implicitly passed.");
        }

        device.startVideo(new VideoHandler(){
            public void onFrameReceived(FrameMode mode, ByteBuffer frame, int timestamp){
            	//System.out.println(mode.getVideoFormat());
            	kinectVideoBuffer = frame;
            }
        });
        
        device.startDepth(new DepthHandler(){
            public void onFrameReceived(FrameMode mode, ByteBuffer frame, int timestamp){
            	kinectDepthBuffer = frame;
            }
        });
	}
	
	/**
	 * Retourne l'ID de la frame.
	 * @param bufferType
	 * 			Si le buffer est vidéo ou de profondeur.
	 * @return L'ID du BufferType, en fonction de si c'est un video buffer ou un depth buffer.
	 */
	int getBufferFrameID(BufferType bufferType){
	    switch(bufferType){
	        case VIDEO: default:
	            return videoBufferFrameID;
	        case DEPTH:
	            return depthBufferFrameID;
	    }
	}
	
	/**
	 * Retourne la hauteur du buffer.
	 * @return La hauteur du buffer.
	 */
    int getBufferHeight(){
    	return bufferHeight;
    }
    
    /**
     * Retourne la largeur du buffer.
     * @return La largeur du buffer.
     */
    int getBufferWidth(){
    	return bufferWidth;
    }

    /**
     * Retourne la couleur au point du kinectVideoBuffer défini en paramètre.
     * @param x
     * 			La valeur de l'abscisse.
     * @param y
     * 			La valeur de l'ordonné.
     * @return La couleur du point.
     */
	ColorRGB getColorAt(int x, int y){
	    // Create the color structure
	    ColorRGB color = new ColorRGB();
	    
	    // Fill it
	    color.R = (int) kinectVideoBuffer.get((x + y*640)*3 + 0) & 255;
	    color.G = (int) kinectVideoBuffer.get((x + y*640)*3 + 1) & 255;
	    color.B = (int) kinectVideoBuffer.get((x + y*640)*3 + 2) & 255;
	    
	    // Return it
	    return color;
	}

	/**
	 * Retourne le buffer de la Kinect en fonction que cela soit un buffer de profondeur ou vidéo.
	 * @param bufferType
	 * 				Si le buffer est de vidéo ou de profondeur.
	 * @return Le buffer de la Kinect.
	 */
	ByteBuffer getKinectBuffer(BufferType bufferType){
	    switch(bufferType){
	        case VIDEO: default:
	            return kinectVideoBuffer;
	        case DEPTH:
	            return kinectDepthBuffer;
	    }
	}

	/**
	 * Retourne la valeur de running.
	 * @return La valeur de running.
	 */
	boolean isRunning(){
	    // If the lib itself shouldn't be running anymore
	    if(!running)
	        return false;
	    
	    // Return true
	    return true;
	}

	/**
	 * Boucle principale de la bibliothèque.
	 */
	void run(){
	    // Get the latest buffers (if we manage to get the buffers, we increase their frame IDs)
	    if(device.getVideoMode() != null) videoBufferFrameID++;
	    if(device.getDepthMode() != null) depthBufferFrameID++;
	}
}
