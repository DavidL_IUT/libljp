package libljp;

import java.awt.image.BufferedImage;

import javax.swing.JFrame;
import javax.swing.JPanel;
/**
 * <b>La classe fenêtre permet de crée une fenetre avec Swing.</b>
 * @author Groupe IHM à l'aide d'un Kinect.
 * @version 1.0
 *
 */
public class Fenetre extends JFrame{
	private static final long serialVersionUID = 1L;
	
	/**
	 * La largeur de la fenêtre.
	 */
	private int windowWidth = 640;
	/**
	 * La hauteur de la fenêtre.
	 */
	private int windowHeight = 480;
	
	/**
	 * Le JPanel qui va tout contenir.
	 */
	private JPanel container;
	/**
	 * L'image.
	 */
	BufferedImage image = new BufferedImage(640, 480, BufferedImage.TYPE_INT_ARGB);
	
	/**
	 * Constructeur Fenetre.
	 * @param libljp
	 * 			La libairie libLjp.
	 */
	public Fenetre(LibLjp libljp){
		this.setTitle("test");
		this.setSize(windowWidth, windowHeight);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		
		container = new TestImagePanel(image, libljp);
		this.setContentPane(container);
		
		this.setVisible(true);
		while(true){
			container.repaint();
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
