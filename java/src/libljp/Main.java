package libljp;
/**
 * <b>La classe Main premet d'exécuter un petit programme de démo.</b>
 * @author Groupe IHM à l'aide d'un Kinect.
 * @version 1.0
 *
 */
public class Main {
    public static void main(String[] args){
    	
    	LibLjp libljp = new LibLjp();
    
    	Fenetre fen = new Fenetre(libljp);
    }
}
