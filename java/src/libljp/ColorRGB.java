package libljp;
/**
 * <b>La classe ColorRGB comprend les 3 couleurs primaires : Rouge, Vert, Bleu.</b>
 * @author Groupe IHM à l'aide d'un Kinect.
 * @version 1.0
 *
 */
public class ColorRGB {
	/**
	 * La valeur du rouge.
	 */
    Integer R;
    
    /**
     * La valeur du vert.
     */
    Integer G;
    
    /**
     * La valeur du bleu.
     */
    Integer B;
    
    /**
     * Constructeur ColorRGB.
     */
    ColorRGB() {
    	R = new Integer(0);
    	G = new Integer(0);
    	B = new Integer(0);
    }
}
