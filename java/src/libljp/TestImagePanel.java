package libljp;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;
import javax.swing.JPanel;
/**
 * <b>La classe TestImagePanel permet d'afficher l'image transmise par la Kinect.</b>
 * @author Groupe IHM à l'aide d'un Kinect.
 * @version 1.0
 *
 */
class TestImagePanel extends JPanel {
	 
	private static final long serialVersionUID = 1L;
	
	/**
	 * L'image.
	 */
	private BufferedImage img;
	
	/**
	 * La libLjp.
	 */
	private LibLjp libljp;
 
	/**
	 * Constructeur TestImagePanel.
	 * @param img
	 * 			L'image en string.
	 * @param libljp
	 * 			La libLjp.
	 */
	public TestImagePanel(String img, LibLjp libljp) {
		this((BufferedImage) new ImageIcon(img).getImage(), libljp);
	}
 
	/**
	 * Constructeur TestImagePanel.
	 * @param img
	 * 			L'image en BufferedImage.
	 * @param libljp
	 * 			La LibLjp.
	 */
	public TestImagePanel(BufferedImage img, LibLjp libljp) {
		this.img = img;
		
		this.libljp = libljp;
	}
	
	/**
	 * Permet d'afficher l'image renvoyer par la Kinect.
	 */
	public void paintComponent(Graphics g) {
		Color color; // RGBA value, each component in a byte
		
		for(int x = 0; x < 640; x++) {
		    for(int y = 0; y < 480; y++) {
		    	color = new Color(libljp.getColorAt(x, y).R, libljp.getColorAt(x, y).G, libljp.getColorAt(x, y).B);
		        img.setRGB(x, y, color.getRGB());
		        if (x==320 && y==240) 
		        	System.out.println(Integer.toBinaryString(libljp.getColorAt(x, y).R) + " " +
		        			Integer.toBinaryString(libljp.getColorAt(x, y).G) + " " +
		        			Integer.toBinaryString(libljp.getColorAt(x, y).B) + " "
		        );
		    }
		}
		
		g.drawImage(img, 0, 0, this);
	}
	
	/**
	 * Retourne la hauteur de l'image.
	 * @param BG
	 * 			L'image.
	 * @return La hauteur.
	 */
	public int getHeight(TestImagePanel BG){
		return img.getHeight(BG);
	}
	/**
	 * Retourne la largeur de l'image.
	 * @param BG
	 * 			L'image.
	 * 
	 * @return La largeur.
	 */
	public int getWidth(TestImagePanel BG){
		return img.getWidth(BG);
	}
}