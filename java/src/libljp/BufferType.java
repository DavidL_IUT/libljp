package libljp;
/**
 * <b>L'enum qui contient les 2 types de buffer.
 * @author Groupe IHM à l'aide d'un Kinect.
 * @version 1.0
 *
 */
public enum BufferType {
	/**
	 * Le type vidéo.
	 */
	VIDEO,
	/**
	 * Le type profondeur.
	 */
	DEPTH
}
