///
///\file LibLjp.hpp
///\brief Fichier LibLjp.hpp, header de la classe LibLjp.
///\author Groupe IHM Kinect
///\version 1.0
///\date 9 Janvier 2014

#ifndef LIBLJP_HPP
#define LIBLJP_HPP

#include <memory>

#include "Color.hpp"
#include "Drawing.hpp"
#include "LjpDevice.hpp"

namespace ljp{

class LibLjp{
    public:
        // Constructor & destructor
        LibLjp();
        ~LibLjp();
        LibLjp(LibLjp &&) = default;
    
        // Public methods
        bool isRunning();
        void run();
        
        // Methods used to obtain the buffers
        int getBufferFrameID(BufferType);
        std::vector<uint8_t>& getKinectBuffer(BufferType);
        int getBufferHeight();
        int getBufferWidth();
        Color getColorAt(int, int);
        
        // Drawing module
        Drawing* getDrawingModule();
        
    private:
        // The device
        LjpDevice* device;
        Freenect::Freenect* freenect;
    
        // Kinect buffer size
        int bufferWidth;
        int bufferHeight;

        // Kinect video & depth buffers
        std::vector<uint8_t> kinectVideoBuffer; // A vector which will receive the LjpDevice video buffer
        std::vector<uint8_t> kinectDepthBuffer; // A vector which will receive the LjpDevice depth buffer
        
        // Video & depth buffers frame IDs
        int videoBufferFrameID;
        int depthBufferFrameID;
        
        // The drawing module
        std::unique_ptr<Drawing> drawingModule;
        
        // Is the lib running?
        bool running;
};

}

#endif
