///
///\file DrawingBlock_Buffers.cpp
///\brief Fichier DrawingBlock_Buffers.cpp, décrit la classe DrawingBlock_Buffers.
///\author Groupe IHM Kinect
///\version 1.0
///\date 9 Janvier 2014

#include "Drawing.hpp"
#include "LibLjp.hpp"

#include "DrawingBlock_Buffers.hpp"
/// \namespace ljp
namespace ljp{

/// \class DrawingBlock_Buffers DrawingBlock_Buffers.hpp
/// \brief Classe représentant un bloc d'affichage qui permet de réprésenter un buffer (vidéo ou de profondeur) à l'écran. Elle hérite de la classe DrawingBlock.

/// \fn DrawingBlock_Buffers::DrawingBlock_Buffers(Drawing& _drawing, int posX, int posY, int width, int height, int bufferWidth, int bufferHeight, BufferType _drawnBufferType, bool _allowEvents)
/// \brief Constructeur de la classe DrawingBlock_Buffers.
/// \param Drawing& _drawing : référence vers le module d'affichage.
/// \param int posX : position x du bloc d'affichage.
/// \param int posY : position y du bloc d'affichage.
/// \param int width : largeur du bloc d'affichage.
/// \param int height : hauteur du bloc d'affichage.
/// \param int bufferWidth : largeur du buffer à afficher.
/// \param int bufferHeight : hauteur du buffer à afficher.
/// \param BufferType _drawnBufferType : type du buffer à afficher.
/// \param bool _allowEvents : si true, les évènements claviers seront activés sur ce bloc d'affichage.
DrawingBlock_Buffers::DrawingBlock_Buffers(Drawing& _drawing, int posX, int posY, int width, int height, int bufferWidth, int bufferHeight, BufferType _drawnBufferType, bool _allowEvents) : DrawingBlock(_drawing, posX, posY, width, height), allowEvents(_allowEvents), drawnBufferType(_drawnBufferType){
    // Set the buffer size
    bufferSize = sf::Vector2i(bufferWidth, bufferHeight);
    
    // We set the sprite position & size
    sprite.setPosition(posX, posY);
    sprite.setScale((float)width/640, (float)height/480);
    
    // We create the texture & set it to the sprite
    texture.create(bufferSize.x, bufferSize.y);
    sprite.setTexture(texture);
}

/// \fn void DrawingBlock_Buffers::draw()
/// \brief Dessine le bloc d'affichage : affiche le buffer à l'écran.
/// \return void
void DrawingBlock_Buffers::draw(){
    // This variable will point to the drawn buffer
    uint8_t* drawnBuffer = drawing.getRGBABuffer(drawnBufferType);
    
    // We update our texture from this buffer
    texture.update(drawnBuffer);

    // And we draw the sprite associated to the texture
    drawing.getWindow().draw(sprite);
}

/// \fn void DrawingBlock_Buffers::handleEvent(sf::Event event)
/// \brief Gestion des évenements clavier relatifs au bloc d'affichage.
/// \param sf::Event event : variable SFML permettant la gestion des évènements.
void DrawingBlock_Buffers::handleEvent(sf::Event event){
    if(allowEvents){
        switch(event.type){
            case sf::Event::KeyPressed:
                switch(event.key.code){
                    case sf::Keyboard::D:
                        drawnBufferType = DEPTH;
                    break;
                    case sf::Keyboard::V:
                        drawnBufferType = VIDEO;
                    break;
                    default: break;
                }
            break;
            default: break;
        }
    }
}

}
