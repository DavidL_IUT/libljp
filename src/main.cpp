///
///\file main.cpp
///\brief Fichier main.cpp, point d'entrée du programme.
///\author Groupe IHM Kinect
///\version 1.0
///\date 9 Janvier 2014
///
///
/// Ce fichier permet à l'utilisateur de la bilbiothèque de tester des exemples de traitements utilisant celle-ci.
#include <iostream>

#include "LibLjp.hpp"

// Exemple de traitement effectué à partir du buffer de profondeur brut (plus rapide car moins d'appels de méthodes ?)
/// \fn void traitementProfondeur(ljp::LibLjp& libLjp)
/// \brief Exemple de traitement sur les données du capteur de profondeur.
/// \param LibLjp libLjp
void traitementProfondeur(ljp::LibLjp& libLjp){
    // Déclaration des variables
    std::vector<uint8_t> bufferProfondeur = libLjp.getKinectBuffer(ljp::DEPTH);
    int largeur = libLjp.getBufferWidth();
    int hauteur = libLjp.getBufferHeight();
    int profondeur = 0;

    // Traitement des données
    for(int i = 0; i < largeur*hauteur; ++i){
        profondeur += bufferProfondeur[i*3];
    }
    profondeur = (int)(profondeur/(largeur*hauteur));
    
    // Affichage
    std::cout << "Traitement 1 (valeur moyenne de profondeur sur le buffer de profondeur) : " << profondeur << std::endl;
}

// Exemple de traitement effectué à l'aide de méthodes simplifiées (plus lent car plus d'appels de méthodes ?)
/// \fn void traitementVideo(ljp::LibLjp& libLjp)
/// \brief Exemple de traitement sur les données du capteur vidéo.
/// \param LibLjp libLjp
void traitementVideo(ljp::LibLjp& libLjp){
    // Déclaration des variables
    int largeur = libLjp.getBufferWidth();
    int hauteur = libLjp.getBufferHeight();
    int vert = 0;
    
    // Traitement des données
    for(int i = 0; i < largeur; ++i){
        for(int j = 0; j < hauteur; ++j){
            vert += libLjp.getColorAt(i, j).G;
        }
    }
    vert = (int)(vert/(largeur*hauteur));
    
    std::cout << "Traitement 2 (valeur moyenne de la couleur verte sur la vidéo) : " << vert << std::endl;
}
/// \fn void boucle(ljp::LibLjp& libLjp)
/// \brief Boucle principale du programme, qui va appeler les autres traitements.
/// \param LibLjp libLjp
void boucle(ljp::LibLjp& libLjp){
    // Variables qui stockent les derniers identifiants de frames
    int dernierIdentifiantVideo = -1;
    int dernierIdentifiantProfondeur = -1;
    
    // Tant que la bilbiothèque ne s'arrête pas
    while(libLjp.isRunning()){
        // On appelle la boucle de traitement de la biliothèque
        libLjp.run();
        
        // Premier exemple de traitement réalisable par l'utilisateur
        if(dernierIdentifiantProfondeur != libLjp.getBufferFrameID(ljp::DEPTH)){
            dernierIdentifiantProfondeur = libLjp.getBufferFrameID(ljp::DEPTH);
            traitementProfondeur(libLjp);
        }
        
        // Deuxième exemple
        if(dernierIdentifiantVideo != libLjp.getBufferFrameID(ljp::VIDEO)){
            dernierIdentifiantVideo = libLjp.getBufferFrameID(ljp::VIDEO);
            traitementVideo(libLjp);
        }
        
        // Et un saut de ligne
        std::cout << std::endl;
    }
}
/// \fn int main()
/// \brief Fonction main.
/// \return Retourne 0 si tout s'est bien déroulé.
int main(){
    // Création de notre instance de la bibliothèque
    ljp::LibLjp libLjp = ljp::LibLjp();
    
    // Démarrage du module d'affichage
    libLjp.getDrawingModule()->start(1024, // La fenêtre aura une largeur de 1024 pixels
                                     768, // Et une hauteur de 768
                                     true); // Et la bibliothèque s'arrêtera à la fermeture de la fenêtre
    
    // Ajout d'un premier module d'affichage BlockBuffers avec les paramètres par défaut : position dans la fenêtre (0, 0), taille (640, 480), affichage de la vidéo par défaut et activation des évènements (on pourra basculer de la vidéo à l'image de profondeur avec les touches d et v)
    libLjp.getDrawingModule()->addBlockBuffers();

    // Ajout d'un deuxième module d'affichage BlockBuffers
    libLjp.getDrawingModule()->addBlockBuffers(640, // Position x de 640 dans la fenêtre
                                               0, // Position y de 0
                                               384, // Largeur de 384
                                               288, // Hauteur de 288
                                               ljp::VIDEO, // Affichage de la vidéo par défaut
                                               false); // Désactivation des évènements (on ne pourra pas basculer de la vidéo à l'image de profondeur avec les touches d et v)

    // Ajout d'autres modules BlockBuffers sous le premier
    libLjp.getDrawingModule()->addBlockBuffers(0, 480, 150, 288);
    libLjp.getDrawingModule()->addBlockBuffers(300, 480, 144, 86);
    libLjp.getDrawingModule()->addBlockBuffers(444, 480, 144, 86);
    libLjp.getDrawingModule()->addBlockBuffers(300, 566, 144, 86);
    libLjp.getDrawingModule()->addBlockBuffers(444, 566, 144, 86);
    
    // La boucle de traitement
    boucle(libLjp);
    
    return 0;
}
