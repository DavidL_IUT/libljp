///
///\file Drawing.cpp
///\brief Fichier Drawing.cpp, décrit la classe Drawing.
///\author Groupe IHM Kinect
///\version 1.0
///\date 9 Janvier 2014

#include "DrawingBlock_Buffers.hpp"
#include "LibLjp.hpp"

#include "Drawing.hpp"
///\namespace ljp
namespace ljp{

/// \class Drawing Drawing.hpp
/// \brief Classe permettant l'affichage à l'écran des données fournies par le périphérique kinect.

/// \fn Drawing::Drawing(LibLjp& _libLjp)
/// \brief Constructeur de la classe Drawing.
/// \param LibLjp& _libLjp : référence vers la variable représentant bilbiothèque.
Drawing::Drawing(LibLjp& _libLjp): libLjp(_libLjp), running(false){

}

/// \fn void Drawing::addBlockBuffers(int x, int y, int width, int height, BufferType defaultBufferType, bool allowEvents)
/// \brief Ajoute un bloc d'affichage permettant d'afficher à l'écran le contenu d'un buffer (vidéo ou de profondeur).
/// \param int posX : position x du bloc d'affichage.
/// \param int posY : position y du bloc d'affichage.
/// \param int width : largeur du bloc d'affichage.
/// \param int height : hauteur du bloc d'affichage.
/// \param int bufferWidth : largeur du buffer à afficher.
/// \param int bufferHeight : hauteur du buffer à afficher.
/// \param BufferType _drawnBufferType : type du buffer à afficher.
/// \param bool _allowEvents : si true, les évènements claviers seront activés sur ce bloc d'affichage.
void Drawing::addBlockBuffers(int x, int y, int width, int height, BufferType defaultBufferType, bool allowEvents){
    // If the width and height are <= 0, we use the window size to set them
    if(width <= 0) width = windowSize.x;
    if(height <= 0) height = windowSize.y;
    
    // Add the block
    drawingBlocks.push_back(std::unique_ptr<DrawingBlock>(new DrawingBlock_Buffers((*this), x, y, width, height, libLjp.getBufferWidth(), libLjp.getBufferHeight(), defaultBufferType, allowEvents)));
}

/// \fn void Drawing::afterDrawing()
/// \brief Fonction appelée en fin d'affichage, elle affiche à l'écran le contenu de la "fenêtre" SFML.
void Drawing::afterDrawing(){
    // Display on the screen all the stuff drawn to the window
    window.display();
}

/// \fn void Drawing::beforeDrawing()
/// \brief Fonction appelée avant tout affichage : elle remplace le contenu de la "fenêtre" SFML par du noir.
void Drawing::beforeDrawing(){
    // Clear the window content in black
    window.clear(sf::Color::Black);
}

/// \fn void Drawing::convertKinectBufferToRGBABuffer(std::vector<uint8_t>& kinectBuffer, uint8_t* rgbaBuffer){
/// \brief Convertit un buffer en un tableau exploitable par la SFML (nécessaire car la SFML n'accepte que les tableaux de pixels RGBA, et pas les RGB).
/// \param std::vector<uint8_t>& kinectBuffer : référence vers le buffer à convertir.
/// \param uint8_t* rgbaBuffer : pointeur vers le buffer RGBA à remplir.
void Drawing::convertKinectBufferToRGBABuffer(std::vector<uint8_t>& kinectBuffer, uint8_t* rgbaBuffer){
    for(int i = 0; i < libLjp.getBufferWidth()*libLjp.getBufferHeight(); i++){
        for(int j = 0; j < 3; j++){
            rgbaBuffer[i*4+j] = kinectBuffer.at(i*3+j);
        }
        rgbaBuffer[i*4+3] = 255;
    }
}

/// \fn uint8_t* Drawing::getRGBABuffer(BufferType bufferType)
/// \brief Retourne un pointeur vers un des deux buffers RGBA (vidéo ou de profondeur), en fonction du type de buffer passé en paramètre.
/// \param BufferType bufferType : le type de buffer
/// \return Retourne un pointeur vers un des deux buffers RGBA (vidéo ou de profondeur).
uint8_t* Drawing::getRGBABuffer(BufferType bufferType){
    switch(bufferType){
        case VIDEO: default:
            return rgbaVideoBuffer;
        break;
        case DEPTH:
            return rgbaDepthBuffer;
        break;
    }
}

/// \fn bool Drawing::getRunning()
/// \brief Retourne true si le module d'affichage est démarré, false sinon.
/// \return Retourne true si le module d'affichage est démarré, false sinon.
bool Drawing::getRunning(){
  return running;
}

/// \fn bool Drawing::getStopRunningTheLibWhenDrawingModuleRunningAndWindowClosed()
/// \brief Retourne true si la bilbiothèque doit se fermer à la fermeture de la fenêtre par l'utilisateur, false sinon.
/// \return Retourne true si la bilbiothèque doit se fermer à la fermeture de la fenêtre par l'utilisateur, false sinon.
bool Drawing::getStopRunningTheLibWhenDrawingModuleRunningAndWindowClosed(){
  return stopRunningTheLibWhenDrawingModuleRunningAndWindowClosed;
}

/// \fn sf::RenderWindow& Drawing::getWindow()
/// \brief Retourne la "fenêtre" SFML.
/// \return Retourne la "fenêtre" SFML.
sf::RenderWindow& Drawing::getWindow(){
  return window;
}

/// \fn void Drawing::handleEvents()
/// \brief Gère des évènements globaux au programme.
void Drawing::handleEvents(){
    while(window.pollEvent(event)){
        // Handle general events
        switch (event.type){
            // If we receive a closed event, then we close the window
            case sf::Event::Closed:
                window.close();
            break;
            // We don't process other events
            default: break;
        }
        
        // Call the handleEvent method of every drawing block
        for(std::list<std::unique_ptr<DrawingBlock> >::iterator it = drawingBlocks.begin(); it != drawingBlocks.end(); ++it){
            (*it)->handleEvent(event);
        }
    }
}

/// \fn bool Drawing::isWindowOpened()
/// \brief Retourne true si la fenêtre est ouverte, false sinon.
/// \return Retourne true si la fenêtre est ouverte, false sinon.
bool Drawing::isWindowOpened(){
    return window.isOpen();
}

/// \fn void Drawing::run()
/// \brief Boucle principale d'affichage.
void Drawing::run(){
    // Handle events
    handleEvents();
    
    // We convert the kinect buffers to RGBA buffers in order to draw them (because SFML textures only accepts RGBA stuff)
    convertKinectBufferToRGBABuffer(libLjp.getKinectBuffer(VIDEO), rgbaVideoBuffer);
    convertKinectBufferToRGBABuffer(libLjp.getKinectBuffer(DEPTH), rgbaDepthBuffer);
    
    // Before drawing
    beforeDrawing();
    
    // Call the drawing method on every block
    for(std::list<std::unique_ptr<DrawingBlock> >::iterator it = drawingBlocks.begin(); it != drawingBlocks.end(); ++it){
        (*it)->draw();
    }
    
    // After drawing
    afterDrawing();
}

/// \fn void Drawing::start(int width, int height, bool _stopRunningTheLibWhenDrawingModuleRunningAndWindowClosed)
/// \brief Démarre le module d'affichage.
/// \param int width : largeur de la fenêtre.
/// \param int height : hauteur de la fenêtre.
/// \param bool _stopRunningTheLibWhenDrawingModuleRunningAndWindowClosed : si true, le bibliothèque sera arrêtée si l'utilisateur ferme la fenêtre. Si false, ça ne sera pas le cas.
void Drawing::start(int width, int height, bool _stopRunningTheLibWhenDrawingModuleRunningAndWindowClosed){
    // If the module isn't running yet
    if(running == false){
        // Set the size from the width and height given in parameters
        windowSize.x = width;
        windowSize.y = height;
        
        // Set the special parameter
        stopRunningTheLibWhenDrawingModuleRunningAndWindowClosed = _stopRunningTheLibWhenDrawingModuleRunningAndWindowClosed;
        
        // Start the module
        running = true;
        
        // Malloc for the rgba buffers (we multiply by 4 because of the 4 R, G, B, A values)
        rgbaVideoBuffer = (uint8_t*)malloc(640*480*4);
        rgbaDepthBuffer = (uint8_t*)malloc(640*480*4);
        
        // Create the window and set the framerate
        window.create(sf::VideoMode(windowSize.x, windowSize.y), "libLJP - SFML drawing");
        window.setFramerateLimit(60); // No framerate limit
    }
}

/// \fn void Drawing::stop()
/// \brief Arrête le module d'affichage.
void Drawing::stop(){
    // If the module is running
    if(running == true){
        // Stop the module
        running = false;
        // Close the window
        window.close();
    }
}

}
