///
///\file DrawingBlock.cpp
///\brief Fichier DrawingBlock.cpp, décrit la classe DrawingBlock.
///\author Groupe IHM Kinect
///\version 1.0
///\date 9 Janvier 2014

#include "LibLjp.hpp"

#include "DrawingBlock.hpp"
/// \namespace ljp
namespace ljp{

/// \class DrawingBlock DrawingBlock.hpp 
/// \brief Classe représentant un bloc d'affichage et disposant d'une position et d'une taille. 

/// \fn DrawingBlock::DrawingBlock(Drawing& _drawing, int posX, int posY, int width, int height)
/// \brief Constructeur de la classe DrawingBlock.
/// \param Drawing& _drawing : référence vers le module d'affichage.
/// \param int posX : position x du bloc d'affichage.
/// \param int posY : position y du bloc d'affichage.
/// \param int width : largeur du bloc d'affichage.
/// \param int height : hauteur du bloc d'affichage.
DrawingBlock::DrawingBlock(Drawing& _drawing, int posX, int posY, int width, int height) : drawing(_drawing){
    // Set the position and the size
    position = sf::Vector2i(posX, posY);
    size = sf::Vector2i(width, height);
}

}
