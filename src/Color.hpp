///
///\file Color.hpp
///\brief Fichier Color.hpp, décrit la structure Color.
///\author Groupe IHM Kinect
///\version 1.0
///\date 9 Janvier 2014

namespace ljp{

struct Color{
    int R;
    int G;
    int B;
};

}
