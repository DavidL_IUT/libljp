///
///\file Mutex.hpp
///\brief Fichier Mutex.hpp, header de la classe Mutex.
///\author Groupe IHM Kinect
///\version 1.0
///\date 9 Janvier 2014

#ifndef MUTEX_HPP
#define MUTEX_HPP

#include <pthread.h>

class Mutex{
    public:
        Mutex();
        
        void lock();
        void unlock();
    
        class ScopedLock{
            public:
                ScopedLock(Mutex & mutex);
                
                ~ScopedLock();
                
            private:
                Mutex &_mutex;
        };
        
    private:
        pthread_mutex_t m_mutex;
};

#endif
