///
///\file Drawing.hpp
///\brief Fichier Drawing.hpp, header de la classe Drawing.
///\author Groupe IHM Kinect
///\version 1.0
///\date 9 Janvier 2014

#ifndef DRAWING_HPP
#define DRAWING_HPP

#include <list>
#include <memory>

#include <SFML/Graphics.hpp>

#include "BufferType.hpp"
#include "DrawingBlock.hpp"

namespace ljp{

class LibLjp;

class Drawing{
    public:
        // Constructor
        Drawing(LibLjp&);
        
        // Public methods
        void addBlockBuffers(int x = 0, int y = 0, int width = 640, int height = 480, BufferType bufferType = VIDEO, bool allowEvents = true); // A a drawing block : the buffers block
        void handleEvents();
        bool isWindowOpened();
        void run(); // Draw the blocks on the window
        void start(int = 640, int = 480, bool = true); // Start the drawing module (create the window)
        void stop(); // Stop the drawing module (delete the window)
        
        // Public getters
        uint8_t* getRGBABuffer(BufferType);
        bool getRunning();
        bool getStopRunningTheLibWhenDrawingModuleRunningAndWindowClosed();
        sf::RenderWindow& getWindow();
        
    private:
        // Variables
        std::list<std::unique_ptr<DrawingBlock> > drawingBlocks; // The drawing blocks
        sf::Event event; // SFML event handler
        LibLjp& libLjp; // Reference to the lib
        bool running; // Is the module running?
        bool stopRunningTheLibWhenDrawingModuleRunningAndWindowClosed; // The variable name is quite explicit
        sf::RenderWindow window; // SFML window
        sf::Vector2i windowSize; // Window size
        uint8_t* rgbaVideoBuffer; // The video buffer (RGBA)
        uint8_t* rgbaDepthBuffer; // The depth buffer (RGBA)
        
        // Drawing methods
        void afterDrawing();
        void beforeDrawing();
        
        // Other private methods
        void convertKinectBufferToRGBABuffer(std::vector<uint8_t>&, uint8_t*);
};

}

#endif
