///
///\file BufferType.hpp
///\brief Fichier BufferType.hpp, décrit l'énumération BufferType.
///\author Groupe IHM Kinect
///\version 1.0
///\date 9 Janvier 2014

#ifndef BUFFERTYPE_HPP
#define BUFFERTYPE_HPP

namespace ljp{

enum BufferType{
    VIDEO,
    DEPTH
};

}

#endif
