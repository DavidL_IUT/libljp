///
///\file LjpDevice.hpp
///\brief Fichier LjpDevice.hpp, header de la classe LjpDevice.
///\author Groupe IHM Kinect
///\version 1.0
///\date 9 Janvier 2014

#ifndef FREENECTDEVICE_HPP
#define FREENECTDEVICE_HPP

#include <vector>

#include "libfreenect.hpp"

#include "Mutex.hpp"

namespace ljp{

class LjpDevice : public Freenect::FreenectDevice{
    public:
        // Constructor
        LjpDevice(freenect_context*, int);
        
        // Public methods
        bool getDepthBuffer(std::vector<uint8_t> &buffer); // Must be called to get the latest depth buffer
        bool getVideoBuffer(std::vector<uint8_t> &buffer); // Must be called to get the latest video buffer
        
        // Public methods we MUST NOT CALL (they're callbacks called by freenect)
        void DepthCallback(void* _depth, uint32_t timestamp); // Will be called by freenect each time a new depth buffer is available
        void VideoCallback(void* _rgb, uint32_t timestamp); // Will be called by freenect each time a new video buffer is available
    
    private:
        // Mutex related stuff
        Mutex depthMutex; // Mutex used for the depth buffer
        Mutex videoMutex; // Mutex used for the video buffer
        std::vector<uint8_t> mutexedDepthBuffer; // The depth buffer (shared between threads!)
        std::vector<uint8_t> mutexedVideoBuffer; // The video buffer (shared between threads!)
        
        // The special gamma buffer, used to convert the content of the depth buffer in something with nice colors readable by a human being
        std::vector<uint16_t> gammaBuffer;
        
        // Two bools used to know if a new frame was produced by freenect since the last call of the getDepthBuffer / getVideoBuffer methods
        // They will be set to true in the DepthCallback and VideoCallback methods, and then set to false in the getDepthBuffer / getVideoBuffer methods
        bool newDepthFrame;
        bool newVideoFrame;
};

}

#endif
