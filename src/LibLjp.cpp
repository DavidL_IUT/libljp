///
///\file LibLjp.cpp
///\brief Fichier LibLjp.cpp, décrit la classe LibLjp.
///\author Groupe IHM Kinect
///\version 1.0
///\date 9 Janvier 2014

#include "LjpDevice.hpp"

#include "LibLjp.hpp"
/// \namespace ljp Namespace principal du projet, utilisé pour toute la bibliothèque.
namespace ljp{

/// \class LibLjp LibLjp.hpp
/// \brief Classe principale de la bibliothèque. Après instanciation par l'utilisateur, elle s'occupe de la récupération des données auprès de la kinect.

/// \fn LibLjp::LibLjp()
/// \brief Constructeur de la classe LibLjp.
LibLjp::LibLjp(): bufferWidth(640), bufferHeight(480), videoBufferFrameID(0), depthBufferFrameID(0), running(true){
    // Create the drawing module (not currently running)
    drawingModule = std::unique_ptr<Drawing>(new Drawing((*this)));
    
    // Create the freenect object
    freenect = new Freenect::Freenect();
    
    // Create the device
    device = &freenect->createDevice<LjpDevice>(0);
    
    // Starting the device
    device->startVideo();
    device->startDepth();
    
    // Resize the buffers
    kinectDepthBuffer.resize(bufferWidth*bufferHeight*3);
    kinectVideoBuffer.resize(bufferWidth*bufferHeight*3);
}

/// \fn LibLjp::~LibLjp()
/// \brief Destructeur de la classe LibLjp.
LibLjp::~LibLjp(){
    // Stopping the device
    device->stopVideo();
    device->stopDepth();
    
    // Delete the device
    delete freenect;
}

/// \fn int LibLjp::getBufferFrameID(BufferType bufferType)
/// \brief Retourne l'identifiant actuel des images vidéo ou de profondeur, selon le type de buffer passé en paramètre.
/// \param BufferType bufferType : type de buffer
/// \return Retourne l'identifiant actuel de l'image.
int LibLjp::getBufferFrameID(BufferType bufferType){
    switch(bufferType){
        case VIDEO: default:
            return videoBufferFrameID;
        break;
        case DEPTH:
            return depthBufferFrameID;
        break;
    }
}

/// \fn Color LibLjp::getColorAt(int x, int y)
/// \brief Retourne la couleur du pixel à la position (x, y)
/// \param int x : position x du pixel
/// \param int y : position y du pixel
/// \return Retourne la couleur du pixel à la position (x, y)
Color LibLjp::getColorAt(int x, int y){
    // Create the color structure
    Color color;
    
    // Fill it
    color.R = kinectVideoBuffer[(x*y)*3];
    color.G = kinectVideoBuffer[(x*y)*3 + 1];
    color.B = kinectVideoBuffer[(x*y)*3 + 2];
    
    // Return it
    return color;
}

/// \fn std::vector<uint8_t>& LibLjp::getKinectBuffer(BufferType bufferType)
/// \brief Retourne le buffer vidéo ou de profondeur, selon le type de buffer passé en paramètre.
/// \param BufferType bufferType : type de buffer
/// \return Retourne le buffer vidéo ou de profondeur.
std::vector<uint8_t>& LibLjp::getKinectBuffer(BufferType bufferType){
    switch(bufferType){
        case VIDEO: default:
            return kinectVideoBuffer;
        break;
        case DEPTH:
            return kinectDepthBuffer;
        break;
    }
}

/// \fn int LibLjp::getBufferHeight()
/// \brief Retourne la hauteur des buffers utilisés par la bibliothèque.
/// \return Retourne la hauteur des buffers utilisés par la bibliothèque.
int LibLjp::getBufferHeight(){
  return bufferHeight;
}

/// \fn int LibLjp::getBufferWidth()
/// \brief Retourne la largeur des buffers utilisés par la bibliothèque.
/// \return Retourne la largeur des buffers utilisés par la bibliothèque.
int LibLjp::getBufferWidth(){
  return bufferWidth;
}

/// \fn Drawing* LibLjp::getDrawingModule()
/// \brief Retourne un pointeur vers le module d'affichage.
/// \return Retourne un pointeur vers le module d'affichage.
Drawing* LibLjp::getDrawingModule(){
  return drawingModule.get();
}

/// \fn bool LibLjp::isRunning()
/// \brief Permet de savoir si la bibliothèque est lancée ou non.
/// \return Retourne true si la bibliothèque est lancée, false sinon.
bool LibLjp::isRunning(){
    // If the lib itself shouldn't be running anymore
    if(!running)
        return false;
    
    // If the drawing module is running but the window is closed and we should stop the lib when the window is closed
    if(drawingModule->getRunning() == true && drawingModule->isWindowOpened() == false && drawingModule->getStopRunningTheLibWhenDrawingModuleRunningAndWindowClosed() == true)
        return false;
    
    // Return true
    return true;
}

/// \fn void LibLjp::run()
/// \brief Fonction appelée par l'utilisateur pour lancer la bibliothèque.
void LibLjp::run(){
    // Get the latest buffers (if we manage to get the buffers, we increase their frame IDs)
    if(device->getVideoBuffer(kinectVideoBuffer)) videoBufferFrameID++;
    if(device->getDepthBuffer(kinectDepthBuffer)) depthBufferFrameID++;
    
    // If the drawing module is running, call its run method
    if(drawingModule->getRunning())
        drawingModule->run();
}

}
