#!/bin/bash

g++ -Wall main.cpp LibLjp.cpp LjpDevice.cpp Mutex.cpp Drawing.cpp DrawingBlock.cpp DrawingBlock_Buffers.cpp -o main -L /usr/local/lib -lfreenect -lm -lsfml-window -lsfml-graphics -std=c++0x
