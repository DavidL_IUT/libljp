///
///\file LjpDevice.cpp
///\brief Fichier LjpDevice.cpp, décrit la classe LjpDevice.
///\author Groupe IHM Kinect
///\version 1.0
///\date 9 Janvier 2014
#include <cmath>

#include "LjpDevice.hpp"

/// \namespace ljp
namespace ljp{

/// \class LjpDevice LjpDevice.hpp
/// \brief Classe chargée d'interagir avec le périphérique Kinect. Elle hérite de la classe FreenectDevice fournie par libFreenect.

/// \fn LjpDevice::LjpDevice(freenect_context *context, int index)
/// \brief Constructeur de la classe LjpDevice.
/// \param freenect_context *context : structure de données propre à libfreenect.
/// \param int index : index du périphérique kinect à utiliser.
LjpDevice::LjpDevice(freenect_context *context, int index) : Freenect::FreenectDevice(context, index),
                                                             mutexedDepthBuffer(freenect_find_video_mode(FREENECT_RESOLUTION_MEDIUM, FREENECT_VIDEO_RGB).bytes),
                                                             mutexedVideoBuffer(freenect_find_video_mode(FREENECT_RESOLUTION_MEDIUM, FREENECT_VIDEO_RGB).bytes),
                                                             gammaBuffer(2048),
                                                             newDepthFrame(false),
                                                             newVideoFrame(false){
    // Fill the gamma buffer
    for(unsigned int i = 0; i < 2048; i++){
        float v = i/2048.0;
        v = std::pow(v, 3)*6;
        gammaBuffer[i] = v*6*256;
    }
}

/// \fn bool LjpDevice::getDepthBuffer(std::vector<uint8_t> &buffer)
/// \brief Récupère l'image de profondeur dans le buffer passé en paramètre, si une nouvelle image est disponible.
/// \param &buffer le buffer
/// \return Retourne true si une nouvelle image était disponible, false sinon.
bool LjpDevice::getDepthBuffer(std::vector<uint8_t> &buffer){
    // Lock the depth mutex
    Mutex::ScopedLock lock(depthMutex);
    
    // If there's no new depth frame, return false
    if(!newDepthFrame)
        return false;
        
    // Else, swap our depth buffer with the one given in parameter
    buffer.swap(mutexedDepthBuffer);
    
    // A new depth frame is available!
    newDepthFrame = false;
    
    // Return true
    return true;
}
/// \fn bool LjpDevice::getVideoBuffer(std::vector<uint8_t> &buffer)
/// \brief Récupère l'image vidéo dans le buffer passé en paramètre, si une nouvelle image est disponible.
/// \param &buffer le buffer
/// \return Retourne true si une nouvelle image était disponible, false sinon.
bool LjpDevice::getVideoBuffer(std::vector<uint8_t> &buffer){
    // Lock the video mutex
    Mutex::ScopedLock lock(videoMutex);
    
    // If there's no new video frame, return false
    if(!newVideoFrame)
        return false;
        
    // Else, swap our video buffer with the one given in parameter
    buffer.swap(mutexedVideoBuffer);
    
    // A new video frame is available!
    newVideoFrame = false;
    
    // Return true
    return true;
}
/// \fn void LjpDevice::DepthCallback(void* _depth, uint32_t timestamp)
/// \brief Callback interne appelé par libFreenect. Ne pas toucher !
/// \param void* _depth
/// \param uint32_t timestamp
void LjpDevice::DepthCallback(void* _depth, uint32_t timestamp){
    Mutex::ScopedLock lock(depthMutex); // Lock the depth mutex
    
    uint16_t* depth = static_cast<uint16_t*>(_depth); // Cast the void pointer given in parameter to a uint16_t pointer
    
    // Fill the depth buffer from the data located at the pointer casted above (with interesting color values obtained thanks to the gamma buffer)
    for(unsigned int i = 0; i < 640*480; i++){
        int pval = gammaBuffer[depth[i]];
        int lb = pval & 0xff;
        switch(pval >> 8){
        case 0:
            mutexedDepthBuffer[3*i+0] = 255;
            mutexedDepthBuffer[3*i+1] = 255-lb;
            mutexedDepthBuffer[3*i+2] = 255-lb;
            break;
        case 1:
            mutexedDepthBuffer[3*i+0] = 255;
            mutexedDepthBuffer[3*i+1] = lb;
            mutexedDepthBuffer[3*i+2] = 0;
            break;
        case 2:
            mutexedDepthBuffer[3*i+0] = 255-lb;
            mutexedDepthBuffer[3*i+1] = 255;
            mutexedDepthBuffer[3*i+2] = 0;
            break;
        case 3:
            mutexedDepthBuffer[3*i+0] = 0;
            mutexedDepthBuffer[3*i+1] = 255;
            mutexedDepthBuffer[3*i+2] = lb;
            break;
        case 4:
            mutexedDepthBuffer[3*i+0] = 0;
            mutexedDepthBuffer[3*i+1] = 255-lb;
            mutexedDepthBuffer[3*i+2] = 255;
            break;
        case 5:
            mutexedDepthBuffer[3*i+0] = 0;
            mutexedDepthBuffer[3*i+1] = 0;
            mutexedDepthBuffer[3*i+2] = 255-lb;
            break;
        default:
            mutexedDepthBuffer[3*i+0] = 0;
            mutexedDepthBuffer[3*i+1] = 0;
            mutexedDepthBuffer[3*i+2] = 0;
            break;
        }
    }
    
    // There's a new depth frame available!
    newDepthFrame = true;
}
/// \fn void LjpDevice::VideoCallback(void* _video, uint32_t timestamp)
/// \brief Callback interne appelé par libFreenect. Ne pas toucher !
/// \param void* _video
/// \param uint32_t timestamp
void LjpDevice::VideoCallback(void* _video, uint32_t timestamp){
    Mutex::ScopedLock lock(videoMutex); // Lock the video mutex
    uint8_t* video = static_cast<uint8_t*>(_video); // Cast the void pointer given in parameter to a uint8_t pointer
    std::copy(video, video + getVideoBufferSize(), mutexedVideoBuffer.begin()); // Copy the data locate at this pointer to our video buffer
    newVideoFrame = true; // There's a new video frame available!
};

}
