///
///\file DrawingBlock.hpp
///\brief Fichier DrawingBlock.hpp, header de la classe DrawingBlock.
///\author Groupe IHM Kinect
///\version 1.0
///\date 9 Janvier 2014

#ifndef DRAWINGBLOCK_HPP
#define DRAWINGBLOCK_HPP

#include <SFML/Graphics.hpp>

namespace ljp{

class Drawing;
class LibLjp;

class DrawingBlock{
    public:
        // Constructor
        DrawingBlock(Drawing&, int, int, int, int);
        
        // Pulic methods
        virtual void draw(){};
        virtual void handleEvent(sf::Event){};
        
    protected:
        // The drawing module
        Drawing& drawing;
    
        // Position and size
        sf::Vector2i position;
        sf::Vector2i size;
};

}

#endif
