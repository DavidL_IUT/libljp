///
///\file DrawingBlock_Buffers.hpp
///\brief Fichier DrawingBlock_Buffers.hpp, header de la classe DrawingBlock_Buffers.
///\author Groupe IHM Kinect
///\version 1.0
///\date 9 Janvier 2014

#ifndef DRAWINGBLOCK_BUFFERS_HPP
#define DRAWINGBLOCK_BUFFERS_HPP

#include <SFML/Graphics.hpp>

#include "BufferType.hpp"
#include "DrawingBlock.hpp"

namespace ljp{

class DrawingBlock_Buffers : public DrawingBlock{
    public:
        // Constructor
        DrawingBlock_Buffers(Drawing&, int, int, int, int, int, int, BufferType, bool);
        
        // Public methods
        void draw();
        void handleEvent(sf::Event);
        
    private:
        bool allowEvents; // If events are allowed the user can switch between one buffer and another
        sf::Vector2i bufferSize; // The buffer size (should be 640*480, the kinect buffer sizes)
        BufferType drawnBufferType; // The type of the buffer which should currently be drawn
        sf::Sprite sprite; // The sprite
        sf::Texture texture; // The texture (we will update it with our buffer)
};

}

#endif

