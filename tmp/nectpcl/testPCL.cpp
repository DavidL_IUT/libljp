#include <boost/thread/thread.hpp>
#include <pcl/common/common_headers.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/PCLPointCloud2.h>

boost::shared_ptr<pcl::visualization::PCLVisualizer> simpleVis(pcl::PCLPointCloud2 cloud){
  boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer(new pcl::visualization::PCLVisualizer("3D Viewer"));
  viewer->setBackgroundColor(0, 0, 0);
  viewer->addPointCloud<pcl::PCLPointCloud2>(cloud, new PointCloudGeometryHandler(cloud), new PointCloudColorHandler(cloud),  "cloud");
  viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 1, "cloud");
  viewer->addCoordinateSystem(1.0);
  viewer->initCameraParameters();
  viewer->setCameraPosition(0, 0, 0, 0, -1, 1);
  return viewer;
}

int main (int argc, char** argv){
  pcl::PCLPointCloud2 ma_chienne;
  
  /*
  // Création du nuage de points
  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_ptr(new pcl::PointCloud<pcl::PointXYZ>);
  
  cloud_ptr->width = (int)cloud_ptr->points.size();
  cloud_ptr->height = 1;
  */

  boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer = simpleVis(ma_chienne);

  // Boucle principale
  while(!viewer->wasStopped()){
    viewer->spinOnce(100);
    boost::this_thread::sleep(boost::posix_time::microseconds(100000));
  }
}
